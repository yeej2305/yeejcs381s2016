#HONOR PLEDGE: the work shown is mine unless otherwise cited
#
#Jonathan Yee
#Lab 11
#21 April 2016
#COMPSCI 381
#

import socket


# Create a "listening socket" that waits for requests:
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)

while True:

  # New client (from Firefox or a terminal "telnet" command):
  tcpSocket,addr = listenSocket.accept()

  # Client sends "GET http://hostname/filename HTTP/1.1" :

  getRequest = tcpSocket.recv(1024)

  # Extract website hostname and the name of the file (lab 9):
  #change variables names to match current sockets

  print "server connected to " + str(addr)
  info = getRequest.split(' ') #separates GET, hostname and filename, and HTTP protocol
  splitinfo = info[1].partition('/') #separates hostname and filename --> http:, another slash, and the hostname and yeejlab10
  realinfo = splitinfo[2].partition('/') #separates the leading slash, the hostname, and the filename with a slash proceeding it
  actualinfo = realinfo[2].partition('/') #splits the
  hostname = actualinfo[0] #string containing hostname
  leadingslash = actualinfo[1] #string containing leading slash
  filename = actualinfo[2] #string containing the filename
  tcpSocket.send('You requested ' + leadingslash + filename+' from '+hostname+'\n\n')
  # Open a new socket to port 80 of the specied hostname:

  webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
  webSocket.connect ((hostname, 80))

  #Convert the hostname/filename into a cache file name
  name = ""+hostname+leadingslash+filename
  cachefilename = name.replace('/','-slash-')
  print "Converting file name to cache file name: "+cachefilename+"\n\n"
  #Check to see if cache file exists in cache directory
  #If file does not exist, send GET request for "/filename" to port 80 of host
  #"hostname", save it in cache directory under correct cache file name and relay
  #contents to the client. Otherwise, return cached file to the client.

  try:
      cachefile = open(cachefilename,'rb')
      cached = True
      print ">The file"+leadingslash+filename+" is in the directory\n"
  except:
      cachefile = open(cachefilename,'wb')
      cached = False
      print ">The file"+leadingslash+filename+" is not in the directory\n"
  if cached:
      print ">Returning file"+leadingslash+filename+" to client\n"
      while True:
        block = cachefile.read(1024)
        if not block:
            break
        tcpSocket.send(block)
      print ">Done\n"
  else:
    print ">Saving file "+leadingslash+filename+" from "+hostname+" to cache directory\n"
  # Send a "GET" request for the file name (use HTTP/1.0 for this);

    webSocket.send('GET '+leadingslash+filename+' HTTP/1.0\n\n')

  # Receive the reply from the web server as a file

    cachefilename = webSocket.makefile('rb', 0)

    while True:
        block = cachefilename.read(1024) #send along tcpsocket to client requested it
        if not block:
            break
        tcpSocket.send(block)

    print ">Done\n"
  tcpSocket.close()
  webSocket.close()
