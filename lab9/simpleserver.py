#HONOR PLEDGE: The work shown is mine unless otherwise cited.
#
#Jonathan Yee
#COMPSCI 381
#Professor Roos
#7 April 2016

# Simple TCP server:
import socket
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.bind(('', 23456))
listenSocket.listen(1)

while True:
  tcpSocket,addr = listenSocket.accept()
  print "server connected to " + str(addr)
  receivedMsg = tcpSocket.recv(1024)
  print "received: " + receivedMsg
  info = receivedMsg.split(' ') #separates string by space
  splitinfo = info[1].partition('/') #splits string into 3 str arrays using '/' as an argument
  realinfo = splitinfo[2].partition('/')
  actualinfo = realinfo[2].partition('/')
  hostname = actualinfo[0]
  leadingslash = actualinfo[1]
  filename = actualinfo[2]
  tcpSocket.send('You requested ' + leadingslash + filename+' from '+hostname)
  tcpSocket.close()
