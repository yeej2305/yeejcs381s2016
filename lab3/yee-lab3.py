# Jonathan Yee
# The work shown is mine unless otherwise cited
# Lab 3


# We need the time package to calculate round-trip times:
import time

from socket import *

host = 'localhost'
port = 12345
timeout = 2 # in seconds

# Create UDP client socket
# Note the use of SOCK_DGRAM for UDP datagram packet
clientsocket = socket(AF_INET, SOCK_DGRAM)
# Set socket timeout as 1 second
clientsocket.settimeout(timeout)

average = 0.0
successes = 0
failures = 0

# Ping 10 times:
for i in range(1,11):
    try:
        message = "Ping " +str(i)
        print "Sending: " + message
	# Save current time (this is the start time):
        start = time.clock()
	# Send the UDP packet containing the ping message
        clientsocket.sendto(message,(host, port))
	# Receive the server response
        modifiedMessage, serverAddress = clientsocket.recvfrom(1024)
	# Save current time (this is the end time)
        end = time.clock()
	# Display the server response as an output
        print "Received message: " + modifiedMessage
        successes += 1

	# print round trip time (difference between end time and start time):
        roundtrip = (end - start)
        average += roundtrip
        print "Round Trip Time: %.10f seconds\n" %roundtrip
    #print number of pings; successful, unsuccessful, and average round-trip 19:10
    # of the successful pings.
    except error:
        #If timeout, assume the packet is lost and continue
        print "request timed out.\n"
        failures +=1
        continue

average = average/10.0
print "Results: \n%d successes, %d failures\naverageRTT = %.10f\n" %(successes,failures,average)
# Close the client socket
clientsocket.close()

