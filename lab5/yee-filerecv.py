#Jonathan Yee
#3 March 2016
#The work shown below is mine unless otherwise cited.
#
#Lab 5
#
#A primitive backup system that is accessed by yee-filesend.py
#Extracts

import socket

# Create a server socket that listens for files:
sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
sock.bind(('',12345))
sock.listen(1)

# Wait at most 30 seconds between files; give up if no more requests.
#sock.settimeout(30)

#Main loop
while True:
    try: # We handle a server socket timeout at the "except:" line

        #Accept connection
        connection,addr = sock.accept()

        #Splits up message to isolate elements
        message = connection.recv(1024)
        name = message.split()[1]
        size = int(message.split()[2])

        #Create a new file:
        f = open('_'+str(name),'wb')

        #Send confirmation message to filesend.py
        connection.send("OK")

        #confirmation of connection
        print "connected to" + str(addr)
        connection.settimeout(1)

        #counter variable to keep track of total received bytes
        counter = 0

        #main loop to read blocks of data
        while counter != size:

            block = connection.recv(1024)

            #writes and saves data into the file
            f.write(block)

            #increment counter
            counter += len(block)

            connection.send(str(counter) + " BYTES")

        # No more data--close file and close connection
        connection.send("OK Received "+str(counter)+" BYTES")
        f.close()
        connection.close()


    except socket.timeout: # connection is still open but no data arriving
        print "connection timed out"
        f.close()
        connection.close()
        sock.close()


