#Jonathan Yee
#3 March 2016
#The work shown below is mine unless otherwise cited.
#
#Lab 5
#
#A primitive backup application that connects to yee-filerecv.py
#Asks the user for the name of a file, computes the size
#of the file, and sends the message.


import socket
import time # for testing purposes--used to force server timeout
import os.path

# Ask user for file name:
filename = raw_input("File to transfer: ")



try:
    f = open(filename,'rb')
    #find the size of the file
    filesize = os.path.getsize(filename)
    print filesize
    # Connect to the server:
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.connect(('',12345))

    sock.send("PUT "+ filename +" " + str(filesize))
    ok = sock.recv(1024)
    print ok

    while True:
      block = f.read(1024)
      # When we reach end of file, block is empty:
      if not block:

        break
      sock.send(block)
      print sock.recv(1024)

    f.close()

    print sock.recv(1024)

    sock.close()

except socket.error:
    print "socket error -- can't find server"

except IOError:
    print "no such file"
