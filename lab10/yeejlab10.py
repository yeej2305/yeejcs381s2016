#HONOR PLEDGE: the work shown is mine unless otherwise cited
#
#Jonathan Yee
#Lab 10
#14 April 2016
#COMPSCI 381
#

import socket


# Create a "listening socket" that waits for requests:
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)

while True:

  # New client (from Firefox or a terminal "telnet" command):
  tcpSocket,addr = listenSocket.accept()

# It's up to you if you want to add a timeout for the listenSocket;
# it's okay to just let it run until you hit CTRL-C
  # Client sends "GET http://hostname/filename HTTP/1.1" :
  getRequest = tcpSocket.recv(1024)
  # Extract website hostname and the name of the file (lab 9):
  #change variables names to match current sockets
  print "server connected to " + str(addr)
  info = getRequest.split(' ') #separates GET, hostname and filename, and HTTP protocol
  splitinfo = info[1].partition('/') #separates hostname and filename --> http:, another slash, and the hostname and yeejlab10
  realinfo = splitinfo[2].partition('/') #separates the leading slash, the hostname, and the filename with a slash proceeding it
  actualinfo = realinfo[2].partition('/') #splits the
  hostname = actualinfo[0] #string containing hostname
  leadingslash = actualinfo[1] #string containing leading slash
  filename = actualinfo[2] #string containing the filename
  tcpSocket.send('You requested ' + leadingslash + filename+' from '+hostname)

  # Open a new socket to port 80 of the specied hostname:

  webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
  webSocket.connect ((hostname, 80))

  # Send a "GET" request for the file name (use HTTP/1.0 for this);
  # don't forget the two '\n\n' at the end of the request!

  webSocket.send('GET '+leadingslash+filename+' HTTP/1.0\n\n')

  # Receive the reply from the web server--it will be a (possibly
  # lengthy) file, so receive it as a file rather than multiple blocks

  requestedFile = webSocket.makefile('rb', 0)
  # Now you can read from this file just lGET http://cs.allegheny.edu/sites/rroos/index.html HTTP/1.1ike any other file and send it
  # back over the tcpSocket; for instance, you can use a loop like
  # the one in "filesend.py" from lab 5
  while True:
      block = requestedFile.read(1024) #send along tcpsocket to client requested it
      if not block:
          break
      tcpSocket.send(block)

  tcpSocket.close()
  webSocket.close()
