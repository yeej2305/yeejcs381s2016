#HONOR PLEDGE: the work shown is mine unless otherwise cited
#
#Jonathan Yee
#Lab 12
#3 May 2016
#COMPSCI 381
#
#
#This modified program has an "expiration timer" that calculates whether a requested file in
#the cache is older than two minutes. If it is, a new version of the file is retrieved
#from the requested host and gets a new copy.

import socket
import time
import os


# Create a "listening socket" that waits for requests:
listenSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
listenSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listenSocket.bind(('', 12345)) # runs on localhost
listenSocket.listen(1)

while True:

  # New client (from Firefox or a terminal "telnet" command):
  tcpSocket,addr = listenSocket.accept()

  # Client sends "GET http://hostname/filename HTTP/1.1" :

  getRequest = tcpSocket.recv(1024)


#####################################################################
#####################################################################

  #Extract website hostname and the name of the file:

  print ">>Server connected to " + str(addr)
  info = getRequest.split(' ') #separates GET, hostname and filename, and HTTP protocol
  splitinfo = info[1].partition('/') #separates hostname and filename --> http:, another slash, and the hostname
  realinfo = splitinfo[2].partition('/') #separates the leading slash, the hostname, and the filename with a slash proceeding it
  actualinfo = realinfo[2].partition('/') #separates the hostname, the leading slash preceding the filename, and the filename
  hostname = actualinfo[0] #string containing hostname
  leadingslash = actualinfo[1] #string containing leading slash
  filename = actualinfo[2] #string containing the filename
  print ">>Requested host: "+hostname+"."
  print ">>Requested file: "+filename+"."

  #Convert the hostname/filename into a cache file name
  name = ""+hostname+leadingslash+filename
  cachefilename = name.replace('/','-slash-')

#####################################################################
#####################################################################


  #Check to see if cache file exists in cache directory
  #If file does not exist, send GET request for "/filename" to port 80 of host
  #"hostname", save it in cache directory under correct cache file name and relay
  #contents to the client. Otherwise, return cached file to the client.

  try:
      cachefile = open(cachefilename,'rb')
      cached = True
      print ">>The file "+leadingslash+filename+" is in the directory\n"
  except:
      cachefile = open(cachefilename,'wb')
      cached = False
      print ">>The file"+leadingslash+filename+" is not in the directory\n"

  if cached:
      ######################################

      # Checks to see if file in cache is "expired". If it is,
      # the server requests a new version of the file. If not,
      # the server returns the cached file to the client.

      currenttime =  time.time()
      oldtime = os.path.getmtime(cachefilename)
      if currenttime - oldtime > 120:
        print "\n>>Cached version too old -- getting newer version\n"
        cachefile.close()
        # Open a new socket to port 80 of the specied hostname:
        webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        webSocket.connect ((hostname, 80))

        # Send a "GET" request for the file name (use HTTP/1.0 for this);
        webSocket.send('GET '+leadingslash+filename+' HTTP/1.0\n\n')

        # Receive the reply from the web server as a file
        remotefile = webSocket.makefile('rb', 0)

        cachefile = open(cachefilename,'wb')
        while True:
            block = remotefile.read(1024) #send along tcpsocket to client requested it
            cachefile.write(block)
            if not block:
                break
            tcpSocket.send(block)

        print ">>Done"
        cachefile.close()

      ######################################

      #Returning file to client

      else:
        print ">>Returning file "+leadingslash+filename+" to client\n"
        cachefile = open(cachefilename,'rb')
        while True:
            block = cachefile.read(1024)
            if not block:
                break
            tcpSocket.send(block)
        cachefile.close()
        print ">>Done\n"

      ######################################
      ######################################

# When file is not in cache

  else:
    print ">Saving file "+leadingslash+filename+" from "+hostname+" to cache directory"

    # Open a new socket to port 80 of the specied hostname:
    webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    webSocket.connect ((hostname, 80))

    # Send a "GET" request for the file name (use HTTP/1.0 for this);
    webSocket.send('GET '+leadingslash+filename+' HTTP/1.0\n\n')

    # Receive the reply from the web server as a file
    remotefile = webSocket.makefile('rb', 0)

    cachefile = open(cachefilename, 'wb')
    while True:
        block = remotefile.read(1024) #send along tcpsocket to client requested it
        cachefile.write(block)
        if not block:
            break
        tcpSocket.send(block)
    cachefile.close()
    webSocket.close()

    print "\n\n>>File saved as: "+cachefilename+"\n"

    print ">Done\n"

  tcpSocket.close()

#####################################################################
#####################################################################

  #END PROGRAM
